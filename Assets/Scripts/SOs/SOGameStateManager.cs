using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;


public enum PointsType
{
    NORMAL,
    BOMB,
}

[CreateAssetMenu(fileName = "MyGameStateManager", menuName = "ScriptableObjects/GameStateManager", order = 51)]
public class SOGameStateManager : ScriptableObject
{
    #region Event Listeners

    public List<SOComponentMatchEventListener> MatchEventListeners =
        new List<SOComponentMatchEventListener>();
    public List<SOComponentLoadSceneEventListener> loadSceneEventListeners =
        new List<SOComponentLoadSceneEventListener>();
    public List<SOComponentEventListener> eventListeners =
        new List<SOComponentEventListener>();

    private void OnEnable()
    {
        foreach (var listenerComponent in MatchEventListeners)
        {
            listenerComponent.OnEnable();
        }
        foreach (var listenerComponent in loadSceneEventListeners)
        {
            listenerComponent.OnEnable();
        }
        foreach (var listenerComponent in eventListeners)
        {
            listenerComponent.OnEnable();
        }
    }

    private void OnDisable()
    {
        foreach (var listenerComponent in MatchEventListeners)
        {
            listenerComponent.OnDisable();
        }
        foreach (var listenerComponent in loadSceneEventListeners)
        {
            listenerComponent.OnDisable();
        }
        foreach (var listenerComponent in eventListeners)
        {
            listenerComponent.OnDisable();
        }
    }

    #endregion

    #region Game State Management

    public int[] pointsMap;
    
    [SerializeField] private int score = 0;

    public SOScoreUpdatedEvent scoreUpdatedEvent;

    public int GetScore()
    {
        return score;
    }

    public void ResetScore()
    {
        score = 0;
    }
    
    public void AddPoints(int sequenceLength, PointsType pointsType)
    {
        score += ComputePointsForMatch(sequenceLength, pointsType);
        scoreUpdatedEvent.Broadcast(score);
    }

    private int ComputePointsForMatch(int sequenceLength, PointsType pointsType)
    {
        Debug.Assert(sequenceLength > 0);
        switch (pointsType)
        {
            case PointsType.NORMAL:
                if (sequenceLength > pointsMap.Length)
                    return pointsMap[pointsMap.Length - 1];
                return pointsMap[sequenceLength - 1];
                break;

            case PointsType.BOMB:
                return sequenceLength * pointsMap[0];
                break;
        }

        Debug.LogError("No case declared for PointsType");
        return 0;
    }

    public void PersistHighScore()
    {
        var highscore = 0;
        if (PlayerPrefs.HasKey("Highscore"))
        {
            highscore = PlayerPrefs.GetInt("Highscore");
        }

        if (score > highscore)
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }

    #endregion
}