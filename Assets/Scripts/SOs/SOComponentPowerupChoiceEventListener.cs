
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// [CreateAssetMenu(fileName = "MySOComponentLoadSceneEventListener", menuName = "ScriptableObjects/SOComponentLoadSceneEventListener", order = 1)] // TODO make it work as an SO. Should a component of an SO be an SO? Probably not in general, because SOs are independent entities you can reference but not own.
[System.Serializable]
public class SOComponentPowerupChoiceEventListener : SOComponent // TODO make it work : SOComponent
{
    
    public SOPowerupChoiceEvent Event;
    public PowerupChoiceCallback Response;
    
    // we are "faking" the event functions
    public override void OnEnable()
    {
        Event.RegisterListener(this);
    }

    public override void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEvent(PowerupEnum chosenPowerup)
    {
        Response?.Invoke(chosenPowerup);
    }
}