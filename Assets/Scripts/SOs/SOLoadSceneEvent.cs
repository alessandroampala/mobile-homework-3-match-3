using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// type for a callback to a LoadSceneEvent
[System.Serializable]
public class LoadSceneCallback : UnityEvent<SceneEnum>
{
}

[CreateAssetMenu(fileName = "MyLoadSceneEvent", menuName = "ScriptableObjects/LoadSceneEvent", order = 1)]
public class SOLoadSceneEvent : ScriptableObject, ISO0AryEvent<MonoLoadSceneEventListener>, ISO0AryEvent<SOComponentLoadSceneEventListener>
{
    
    // TODO this duplication is because I haven't figured out how to
    // make MonoLoadSceneEventListener and SOComponentLoadSceneEventListener
    // two things that use/inherit from/are the same LoadSceneEventListener
    // (with the only difference that the first one is a component and the
    // second is an SOComponent for ComposableScriptableObjects)
    public List<MonoLoadSceneEventListener> monoListeners = new List<MonoLoadSceneEventListener>();
    public List<SOComponentLoadSceneEventListener> SOComponentListeners = new List<SOComponentLoadSceneEventListener>();

    public SceneEnum sceneToLoad;

    public void Broadcast()
    {
        for (int i = monoListeners.Count - 1; i >= 0; i--)
        {
            // we iterate backwards so that if a listeners unregisters
            // itself, we don't have iteration problems

            monoListeners[i].OnEvent(sceneToLoad);
        }
        for (int i = SOComponentListeners.Count - 1; i >= 0; i--)
        {
            Debug.Log($"{name} - Sending {sceneToLoad} to {SOComponentListeners[i]}");
            SOComponentListeners[i].OnEvent(sceneToLoad);
        }
    }

    public void RegisterListener(MonoLoadSceneEventListener listener)
    {
        if (!monoListeners.Contains(listener))
            monoListeners.Add(listener);
    }
    
    // TODO this duplication is because I haven't figured out how to
    // make MonoLoadSceneEventListener and SOComponentLoadSceneEventListener
    // two things that use/inherit from/are the same LoadSceneEventListener
    // (with the only difference that the first one is a component and the
    // second is an SOComponent for ComposableScriptableObjects)
    public void RegisterListener(SOComponentLoadSceneEventListener listener)
    {
        if (!SOComponentListeners.Contains(listener))
        {
            SOComponentListeners.Add(listener);
            Debug.Log($"{name} ({sceneToLoad}) - Registering {listener}. (Total: {SOComponentListeners.Count}");
        }
    }

    public void UnregisterListener(MonoLoadSceneEventListener listener)
    {
        if (monoListeners.Contains(listener))
            monoListeners.Remove(listener);
    }
    
    // TODO this duplication is because I haven't figured out how to
    // make MonoLoadSceneEventListener and SOComponentLoadSceneEventListener
    // two things that use/inherit from/are the same LoadSceneEventListener
    // (with the only difference that the first one is a component and the
    // second is an SOComponent for ComposableScriptableObjects)
    public void UnregisterListener(SOComponentLoadSceneEventListener listener)
    {
        if (SOComponentListeners.Contains(listener))
            SOComponentListeners.Remove(listener);
    }
}
