using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyCinematicManager", menuName = "ScriptableObjects/CinematicManager", order = 51)]
public class SOCinematicManager : ScriptableObject
{

    #region Event Listeners

    public List<SOComponentCinematicEventListener> cinameticEventListeners =
        new List<SOComponentCinematicEventListener>();

    private void OnEnable()
    {
        foreach (var listenerComponent in cinameticEventListeners)
        {
            listenerComponent.OnEnable();
        }
    }

    private void OnDisable()
    {
        foreach (var listenerComponent in cinameticEventListeners)
        {
            listenerComponent.OnDisable();
        }
    }

    #endregion

    #region Cinematic Management

    public void FadeInOverlay()
    {
        
    }

    #endregion
}