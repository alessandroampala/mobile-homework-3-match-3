using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerupEnum
{
    BOMB,
    FREEZE
}

[CreateAssetMenu(fileName = "MyPowerupManager", menuName = "ScriptableObjects/PowerupManager", order = 51)]
public class SOPowerupManager : ScriptableObject
{

    #region Event Listeners

    public List<SOComponentPowerupChoiceEventListener> powerupChoiceEventListeners =
        new List<SOComponentPowerupChoiceEventListener>();

    private void OnEnable()
    {
        foreach (var listenerComponent in powerupChoiceEventListeners)
        {
            listenerComponent.OnEnable();
        }
    }

    private void OnDisable()
    {
        foreach (var listenerComponent in powerupChoiceEventListeners)
        {
            listenerComponent.OnDisable();
        }
    }

    #endregion

    #region Powerup Management

    public PowerupEnum selectedPowerup;

    public void SetPowerup(PowerupEnum chosenPowerup)
    {
        selectedPowerup = chosenPowerup;
    }

    public PowerupEnum GetSelectedPowerup()
    {
        return selectedPowerup;
    }

    #endregion
}