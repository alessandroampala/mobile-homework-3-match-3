using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// type for a callback to a PowerupChoiceEvent
[System.Serializable]
public class PowerupChoiceCallback : UnityEvent<PowerupEnum>
{
}

[CreateAssetMenu(fileName = "MyPowerupChoiceEvent", menuName = "ScriptableObjects/PowerupChoiceEvent", order = 1)]
public class SOPowerupChoiceEvent : ScriptableObject, ISO0AryEvent<SOComponentPowerupChoiceEventListener>
{
    private List<SOComponentPowerupChoiceEventListener> SOComponentListeners = new List<SOComponentPowerupChoiceEventListener>();

    public PowerupEnum chosenPowerup;

    public void Broadcast()
    {
        for (int i = SOComponentListeners.Count - 1; i >= 0; i--)
        {
            SOComponentListeners[i].OnEvent(chosenPowerup);
        }
    }

    public void RegisterListener(SOComponentPowerupChoiceEventListener listener)
    {
        if (!SOComponentListeners.Contains(listener))
            SOComponentListeners.Add(listener);
    }
  
    public void UnregisterListener(SOComponentPowerupChoiceEventListener listener)
    {
        if (SOComponentListeners.Contains(listener))
            SOComponentListeners.Remove(listener);
    }
}
