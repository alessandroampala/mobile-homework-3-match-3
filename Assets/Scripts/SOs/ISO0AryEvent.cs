

public interface ISO0AryEvent<in T>
{
    public void Broadcast();
    public void RegisterListener(T listener);
    public void UnregisterListener(T listener);
}