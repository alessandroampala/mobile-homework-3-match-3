using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// type for a callback to a PowerupChoiceEvent
[System.Serializable]
public class CinematicCallback : UnityEvent
{
}

[CreateAssetMenu(fileName = "MyCinematicEvent", menuName = "ScriptableObjects/CinematicEvent", order = 1)]
public class SOCinematicEvent : ScriptableObject, ISO0AryEvent<SOComponentCinematicEventListener>
{
    private List<SOComponentCinematicEventListener> SOComponentListeners = new List<SOComponentCinematicEventListener>();

    public void Broadcast()
    {
        for (int i = SOComponentListeners.Count - 1; i >= 0; i--)
        {
            SOComponentListeners[i].OnEvent();
        }
    }

    public void RegisterListener(SOComponentCinematicEventListener listener)
    {
        if (!SOComponentListeners.Contains(listener))
            SOComponentListeners.Add(listener);
    }
  
    public void UnregisterListener(SOComponentCinematicEventListener listener)
    {
        if (SOComponentListeners.Contains(listener))
            SOComponentListeners.Remove(listener);
    }
}
