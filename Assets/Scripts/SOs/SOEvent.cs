using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyEvent", menuName = "ScriptableObjects/Event", order = 1)]
public class SOEvent : ScriptableObject, ISO0AryEvent<MonoEventListener>, ISO0AryEvent<SOComponentEventListener>
{
    public List<MonoEventListener> listeners = new List<MonoEventListener>();
    public List<SOComponentEventListener> SOComponentListeners = new List<SOComponentEventListener>();

    public void Broadcast()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            // we iterate backwards so that if a listeners unregisters
            // itself, we don't have iteration problems

            listeners[i].OnEvent();
        }
        for (int i = SOComponentListeners.Count - 1; i >= 0; i--)
        {
            SOComponentListeners[i].OnEvent();
        }
    }

    public void RegisterListener(SOComponentEventListener listener)
    {
        if (!SOComponentListeners.Contains(listener))
            SOComponentListeners.Add(listener);
    }

    public void UnregisterListener(SOComponentEventListener listener)
    {
        if (SOComponentListeners.Contains(listener))
            SOComponentListeners.Remove(listener);
    }

    public void RegisterListener(MonoEventListener listener)
    {
        if (!listeners.Contains(listener))
            listeners.Add(listener);
    }

    public void UnregisterListener(MonoEventListener listener)
    {
        if (listeners.Contains(listener))
            listeners.Remove(listener);
    }
}
