
using System;
using System.Collections.Generic;
using UnityEngine;

// A ScriptableObject with components that one can
// add and remove at editing time from the inspector.
// Analogous of GameObject (which is an object that has
// lifecycle methods, usually lives in the scene, and is
// made of arbitrary components).
public class ComposableScriptableObject : ScriptableObject
{
    // TODO it shouldn't actually be an exposed and serialized list
    // of components, but rather there should be a Custom Editor
    // for ComposableScriptableObject that adds the possibility of
    // adding SOComponents (much like for normal GameObjects)
    public List<SOComponent> components = new List<SOComponent>();

    #region Event Functions
    
    protected virtual void OnEnable()
    {
        foreach (var component in components)
        {
            component.OnEnable();
        }
    }

    protected virtual void OnDisable()
    {
        foreach (var component in components)
        {
            component.OnDisable();
        }
    }

    #endregion
}