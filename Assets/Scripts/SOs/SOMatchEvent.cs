using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MatchCallback : UnityEvent<int, PointsType>
{
}

[CreateAssetMenu(fileName = "MyMatchEvent", menuName = "ScriptableObjects/MatchEvent", order = 1)]
public class SOMatchEvent : ScriptableObject
{
    private List<SOComponentMatchEventListener> SOComponentListeners =
        new List<SOComponentMatchEventListener>();
    private List<MonoMatchEventListener> monoListeners =
        new List<MonoMatchEventListener>();

    public void Broadcast(int sequenceLength, PointsType pointsType)
    {
        for (int i = SOComponentListeners.Count - 1; i >= 0; i--)
        {
            SOComponentListeners[i].OnEvent(sequenceLength, pointsType);
        }
        for (int i = monoListeners.Count - 1; i >= 0; i--)
        {
            monoListeners[i].OnEvent(sequenceLength, pointsType);
        }
    }

    public void RegisterListener(SOComponentMatchEventListener listener)
    {
        if (!SOComponentListeners.Contains(listener))
            SOComponentListeners.Add(listener);
    }

    public void UnregisterListener(SOComponentMatchEventListener listener)
    {
        if (SOComponentListeners.Contains(listener))
            SOComponentListeners.Remove(listener);
    }
    
    public void RegisterListener(MonoMatchEventListener listener)
    {
        if (!monoListeners.Contains(listener))
            monoListeners.Add(listener);
    }

    public void UnregisterListener(MonoMatchEventListener listener)
    {
        if (monoListeners.Contains(listener))
            monoListeners.Remove(listener);
    }
}
