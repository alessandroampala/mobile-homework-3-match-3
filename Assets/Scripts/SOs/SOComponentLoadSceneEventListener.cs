
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


// Analogous of MonoLoadSceneEventListener (which one could read as
// "the event listener component for GameObjects). In this case,
// this should be considered as "the event listener component
// for ComposableScriptableObjects".

// [CreateAssetMenu(fileName = "MySOComponentLoadSceneEventListener", menuName = "ScriptableObjects/SOComponentLoadSceneEventListener", order = 1)] // TODO make it work as an SO. Should a component of an SO be an SO? Probably not in general, because SOs are independent entities you can reference but not own.
[System.Serializable]
public class SOComponentLoadSceneEventListener : SOComponent // TODO make it work : SOComponent
{
    
    // public List<SOLoadSceneEventListener> loadSceneEventListeners = new List<SOLoadSceneEventListener>();
    public SOLoadSceneEvent Event;
    public LoadSceneCallback Response;
    
    // we are "faking" the event functions
    public override void OnEnable()
    {
        Event.RegisterListener(this);
    }

    public override void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEvent(SceneEnum sceneEnum)
    {
        Response?.Invoke(sceneEnum);
    }
}