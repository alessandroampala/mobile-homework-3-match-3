// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.SceneManagement;
// using UnityEngine.SocialPlatforms.Impl;
//
//
//
// public class SOGenericEventListener<SOCompListenerT, MonoListenerT> 
//     : ScriptableObject
//     where SOCompListenerT : SOComponent
//     where MonoListenerT : MonoBehavior // TODO this breaks, not sure why
// {
//     public List<SOCompListenerT> SOComponentListeners =
//         new List<SOCompListenerT>();
//
//     public List<MonoListenerT> MonoListeners =
//         new List<MonoListenerT>();
//
//     private void OnEnable()
//     {
//         foreach (var listenerComponent in SOComponentListeners)
//         {
//             listenerComponent.OnEnable();
//         }
//
//         foreach (var listenerComponent in MonoListeners)
//         {
//             listenerComponent.OnEnable();
//         }
//     }
//
//     private void OnDisable()
//     {
//         foreach (var listenerComponent in SOComponentListeners)
//         {
//             listenerComponent.OnDisable();
//         }
//
//         foreach (var listenerComponent in MonoListeners)
//         {
//             listenerComponent.OnDisable();
//         }
//     }
// }