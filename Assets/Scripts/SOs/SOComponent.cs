

using UnityEngine;

// Objects of this type can be added as "components" to ComposableScriptableObjects
// Analogous of components (MonoBehavior) for ScriptableObjects (instead of GameObjects)
[System.Serializable]
public class SOComponent // TODO could this be an SO? should it be? probably not in general.
{
    // event functions 
    // TODO it would be nice to implement the same way event functions
    // like Update are implemented in Unity. See:
    // https://answers.unity.com/questions/966269/what-exactly-is-going-on-when-i-implement-update-a.html
    // This way, we wouldn't need to necessarily implement all the
    // functions defined by this interface. (And they could be private again).

    public virtual void OnEnable()
    {
    } // TODO unfortunately we can't enforce methods to be private with a C# interface?

    public virtual void OnDisable()
    {
    }
}