using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Object = System.Object;

public enum SceneEnum
{
    MAIN_MENU,
    GAME_SCENE,
    POWERUP_CHOICE_SCENE,
    GAME_OVER_SCENE
}

[System.Serializable]
public class SceneField
{
    public SceneEnum sceneEnum;
    public SerializableScene scene;
}

// TODO we use this class to serialize a list of couples
// (we can't serialize a dict). This is only temporary, and
// would not be needed if we managed to make
// SOComponentLoadSceneEventListener work
// [System.Serializable]
// public class SOLoadSceneEventListener
// {
//     public SOLoadSceneEvent Event;
//     public LoadSceneCallback Response;
//
//     public void _RegisterItself()
//     {
//         Event.RegisterListener(this);
//     }
// }

[CreateAssetMenu(fileName = "MySceneManager", menuName = "ScriptableObjects/SceneManager", order = 51)]
public class
    SOSceneManager : ScriptableObject // ComposableScriptableObject TODO make it a ComposableScriptableObject and make it work
{
    #region Event Listeners

    // TODO for now we hardcode here all the code for the event listeners
    // but ideally this class should be a ComposableScriptableObject
    // that can take any number and types of components, and
    // we would add several SOComponentEventListener - one for each event -
    // to it. Much like we do with EventListener components on GameObjects

    // TODO problem: I can't serialize SOComponent correctly (they are polymorphic, since 
    // I would want a list of a base class that I can then fill with derived classes).
    // But maybe before that, the problem is that components shouldn't be set dynamically
    // (they are not independent objects that I can reference like in a normal serialized
    // Object field. Rather, they are additional pieces of code that I can add from
    // the custom menu, like we do for components on GameObjects).

    public List<SOComponentLoadSceneEventListener> loadSceneEventListeners =
        new List<SOComponentLoadSceneEventListener>();

    public List<SOComponentEventListener> eventListeners = 
        new List<SOComponentEventListener>();

    private void OnEnable()
    {
        foreach (var listenerComponent in loadSceneEventListeners)
        {
            listenerComponent.OnEnable();
        }

        foreach (var listenerComponent in eventListeners)
        {
            listenerComponent.OnEnable();
        }
    }

    private void OnDisable()
    {
        foreach (var listenerComponent in loadSceneEventListeners)
        {
            listenerComponent.OnDisable();
        }

        foreach (var listenerComponent in eventListeners)
        {
            listenerComponent.OnDisable();
        }
    }

    #endregion

    #region Scene Management

    public List<SceneField> allScenes = new List<SceneField>();

    public Animator transitionAnimationIn;
    public Animator transitionAnimationOut;

    private SceneEnum? sceneBeingLoaded;

    public void StartLoadingScene(SceneEnum sceneEnum)
    {
        Debug.Log($"{name} - Received event : {sceneEnum}");
        var canvas = FindObjectOfType<Canvas>();
        var transitionAnimationInInstance = Instantiate(transitionAnimationIn, canvas.transform);

        // save the scene to be loaded and wait for the transition to end
        sceneBeingLoaded = sceneEnum;
        // once the animation ends, it will trigger an event that will be caught here,
        // and NowLoadScene will be called, doing the actual loading
    }

    public void NowLoadScene()
    {
        // Debug.Log($"{name} - now loading scene");
        if (sceneBeingLoaded.HasValue)
        {
            var lookup = allScenes.ToDictionary(s => s.sceneEnum, s => s.scene);
            SceneManager.LoadScene(lookup[sceneBeingLoaded.Value]);
        }
        else
        {
            Debug.LogError($"{name} - Trying to load scene but no scene was set as the one to load.");
        }
        sceneBeingLoaded = null;
    }

    #endregion
}