using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyScoreUpdatedEvent", menuName = "ScriptableObjects/ScoreUpdatedEvent", order = 1)]
public class SOScoreUpdatedEvent : ScriptableObject
{
    private List<MonoScoreUpdatedListener> listeners = new List<MonoScoreUpdatedListener>();

    public void Broadcast(int newScore)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEvent(newScore);
        }
    }

    public void RegisterListener(MonoScoreUpdatedListener listener)
    {
        if (!listeners.Contains(listener))
            listeners.Add(listener);
    }

    public void UnregisterListener(MonoScoreUpdatedListener listener)
    {
        if (listeners.Contains(listener))
            listeners.Remove(listener);
    }
}
