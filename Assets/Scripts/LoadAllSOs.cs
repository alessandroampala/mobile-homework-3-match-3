using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAllSOs : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Resources.LoadAll("SO Instances");
    }
}
