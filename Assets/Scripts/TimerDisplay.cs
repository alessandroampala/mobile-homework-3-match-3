using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class TimerDisplay : MonoBehaviour
{
    public float timer = 2 * 60; //time kept in seconds
    private Queue<float> freezes = new Queue<float>();
    private TextMeshProUGUI text;
    public UnityEvent OnTimerEnd;

    void Start()
    {
        StartCoroutine(Tick());
        text = GetComponent<TextMeshProUGUI>();
        text.text = ToString();
    }

    public void Freeze(float freezeTime)
    {
        Debug.Assert(freezeTime > 0);
        freezes.Enqueue(freezeTime);
    }

    private IEnumerator Tick()
    {
        WaitForSeconds second = new WaitForSeconds(1);

        while(timer > 0)
        {
            yield return second;

            while(freezes.Count > 0)
            {
                //update freeze graphics
                yield return new WaitForSeconds(freezes.Dequeue());
            }

            timer--;
            //UPDATE TIMER GRAPHICS
            text.text = ToString();
        }

        //Throw timer ended event
        OnTimerEnd?.Invoke();
    }

    public override string ToString()
    {
        return TimeSpan.FromSeconds(timer).ToString("mm':'ss");
    }
}
