using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(TMPro.TextMeshProUGUI))]
public class HighscoreLabel : MonoBehaviour
{
    private TMPro.TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();

        var highscore = 0;
        if (PlayerPrefs.HasKey("Highscore"))
        {
            highscore = PlayerPrefs.GetInt("Highscore");
        }

        text.text = highscore.ToString();
    }
}
