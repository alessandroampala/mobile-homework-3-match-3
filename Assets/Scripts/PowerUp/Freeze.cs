using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freeze : MonoBehaviour, IPowerUp
{
    public List<Coord> Activate(List<Coord> matchSequence, GridManager grid)
    {
        grid.timer.Freeze(3);
        return null;
    }
}
