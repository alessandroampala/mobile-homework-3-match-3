using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Bomb : MonoBehaviour, IPowerUp
{
    /* The effect of the PowerUp.
     * Takes the matchSequence array and returns
     * the Coords of the additional pieces to be destroyed
     */
    public List<Coord> Activate(List<Coord> matchSequence, GridManager grid)
    {
        Piece thisPiece = GetComponent<Piece>();
        int x = (int)thisPiece.gridPos.x;
        int y = (int)thisPiece.gridPos.y;

        //calculate explosion pieces
        List<Coord> explodedPieces = new List<Coord>();
        AddToListIfInGrid(x - 1, y + 1, explodedPieces, grid);
        AddToListIfInGrid(x, y + 1, explodedPieces, grid);
        AddToListIfInGrid(x + 1, y + 1, explodedPieces, grid);
        AddToListIfInGrid(x - 1, y, explodedPieces, grid);
        AddToListIfInGrid(x + 1, y, explodedPieces, grid);
        AddToListIfInGrid(x - 1, y - 1, explodedPieces, grid);
        AddToListIfInGrid(x, y - 1, explodedPieces, grid);
        AddToListIfInGrid(x + 1, y - 1, explodedPieces, grid);

        //subtract from matchSequence if the pieces already has to be destroyed
        return explodedPieces.Except(matchSequence).ToList();
    }

    private void AddToListIfInGrid(int x, int y, List<Coord> list, GridManager grid)
    {
        if (grid.IsInGrid(x, y))
            list.Add(new Coord((ushort) x, (ushort) y));
    }

}
