using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPowerUp
{
    /* The effect of the PowerUp.
     * Takes the matchSequence array and returns
     * the Coords of the additional pieces to be destroyed
     */
    public List<Coord> Activate(List<Coord> matchSequence, GridManager grid);
}
