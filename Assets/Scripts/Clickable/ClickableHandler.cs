using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Input management
// This script checks and calls methods on IClickable objects
public class ClickableHandler : MonoBehaviour
{
    public static bool active = true;

    private Camera mainCamera;

    void Start()
    {
        // TODO dependencies better as explicit and serialized?
        mainCamera = Camera.main;
    }

    void Update()
    {
        if(active)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                CastRayKeyUp();
            }
            else
            {
                // TODO use 2 else if instead of nesting ifs?
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    CastRayKeyDown();
                }
                else if (Input.GetKey(KeyCode.Mouse0))
                    CastRayPressing();
            }
        }
    }

    #region CastRay

    // TODO grammar: hit, not hitted
    private GameObject lastHitted;
    private GameObject firstHitted;
    private IClickable firstHittedClickable;

    void CastRayKeyUp()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit && hit == firstHitted)
        {
            firstHittedClickable.Click();
        }
    }

    void CastRayPressing()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        Pressing(hit);
    }

    void CastRayKeyDown()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            firstHitted = hit.collider.gameObject;
            firstHittedClickable = firstHitted.GetComponent<IClickable>();
            Pressing(hit);
        }
    }

    void Pressing(RaycastHit2D hit)
    {
        if (hit && hit.collider.gameObject == firstHitted)
        {
            if (hit.collider.gameObject != lastHitted && lastHitted != null)
            {
                lastHitted.gameObject.GetComponent<IClickable>().EndPressing();
            }
            lastHitted = hit.collider.gameObject;
            firstHittedClickable.Pressing(hit.point);
        }
        else
        {
            if (lastHitted != null)
            {
                lastHitted.gameObject.GetComponent<IClickable>().EndPressing();
                lastHitted = null;
            }
            firstHitted = null;
            firstHittedClickable = null;
        }
    }

    #endregion
}
