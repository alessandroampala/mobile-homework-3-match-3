﻿using UnityEngine;

public interface IClickable
{
    public abstract void Click();
    public abstract void Pressing(Vector2 pos);
    public abstract void EndPressing();
}
