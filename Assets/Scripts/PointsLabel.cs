using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(TMPro.TextMeshProUGUI))]
public class PointsLabel : MonoBehaviour
{
    private TMPro.TextMeshProUGUI text;
    public SOGameStateManager gameStateManager;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();
        
        UpdateLabel(gameStateManager.GetScore());
    }

    public void UpdateLabel(System.Int32 newScore)
    {
        text.text = newScore.ToString();
    }
}
