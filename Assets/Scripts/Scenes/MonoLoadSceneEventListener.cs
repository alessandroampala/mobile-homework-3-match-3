using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class MonoLoadSceneEventListener : MonoBehaviour
{
    // what message do we listen for?
    // (we don't listen for a particular emitter, but for an event)
    public SOLoadSceneEvent Event;
    
    // serialized response
    public LoadSceneCallback Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEvent(SceneEnum sceneEnum)
    {
        Response?.Invoke(sceneEnum);
    }
}