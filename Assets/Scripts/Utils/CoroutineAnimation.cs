using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineAnimation : MonoBehaviour
{
    //singleton, instance of MonoBehaviour needed for a coroutine to be executed
    private static CoroutineAnimation instance;
    public static CoroutineAnimation Instance
    {
        get { return instance; }
        private set { }
    }

    void Awake()
    {
        instance = this;
    }

    public static void SmoothStepToTarget(Transform objTransform, Vector3 targetPosition, float duration)
    {
        instance.StartCoroutine(SmoothStepToTargetCoroutine(objTransform, targetPosition, duration));
    }

    //version with callback
    public static void SmoothStepToTarget(Transform objTransform, Vector3 targetPosition, float duration, System.Action endAction)
    {
        instance.StartCoroutine(SmoothStepToTargetCoroutine(objTransform, targetPosition, duration, endAction));
    }

    // Smoothly move object towards targetPosition, and the time it takes is duration
    private static IEnumerator SmoothStepToTargetCoroutine(Transform objTransform, Vector3 targetPosition, float duration)
    {
        Vector3 startPosition = objTransform.position;
        float lerp = 0;
        while (lerp < 1 && duration > 0)
        {
            if (objTransform == null) yield break;
            lerp = Mathf.MoveTowards(lerp, 1, Time.deltaTime / duration);
            float smoothLerp = Mathf.SmoothStep(0, 1, lerp);
            objTransform.position = Vector3.Lerp(startPosition, targetPosition, smoothLerp);
            yield return null;
        }

        objTransform.position = targetPosition;
    }

    private static IEnumerator SmoothStepToTargetCoroutine(Transform objTransform, Vector3 targetPosition, float duration, System.Action endAction)
    {
        Vector3 startPosition = objTransform.position;
        float lerp = 0;
        while (lerp < 1 && duration > 0)
        {
            if (objTransform == null) yield break;
            lerp = Mathf.MoveTowards(lerp, 1, Time.deltaTime / duration);
            float smoothLerp = Mathf.SmoothStep(0, 1, lerp);
            objTransform.position = Vector3.Lerp(startPosition, targetPosition, smoothLerp);
            yield return null;
        }

        objTransform.position = targetPosition;
        endAction.Invoke();
    }
}
