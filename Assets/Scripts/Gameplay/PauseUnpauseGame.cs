using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseUnpauseGame : MonoBehaviour
{
    public bool isGamePaused { get; private set; }

    public void PauseGame()
    {
        isGamePaused = true;
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        isGamePaused = false;
        Time.timeScale = 1;
    }
}
