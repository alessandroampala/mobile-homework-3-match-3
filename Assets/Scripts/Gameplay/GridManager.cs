using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

//
public enum PieceType
{
    WHITE,
    RED,
    GREEN,
    PURPLE,
    ORANGE,
    BLUE,

    BOMB,
    FREEZE,
}

[System.Serializable]
public struct PiecePrefab
{
    public GameObject prefab;
    public PieceType type;
}

//Used to represent match positions in MatchResult struct
public struct Coord
{
    public readonly ushort x;
    public readonly ushort y;

    public Coord(ushort x, ushort y)
    {
        this.x = x;
        this.y = y;
    }
}

public class GridManager : MonoBehaviour
{
    public int rows = 8, columns = 8;

    private static GridManager instance;
    public static GridManager Instance
    {
        get { return instance; }
        private set { }
    }

    public PiecePrefab[] prefabs;
    public PiecePrefab[] powerUpPrefabs;
    [HideInInspector] public Piece[,] pieces; //the board
    private PieceType currentPowerUp;
    public SOPowerupManager powerupManager;
    public SOMatchEvent matchEvent;
    [Range(0,100)] public float powerUpProbability = 5f; //5% prob
    public TimerDisplay timer;

    List<PieceType> powerUpTypes = new List<PieceType>();
    Dictionary<PieceType, GameObject> prefabsDict;
    Dictionary<PieceType, GameObject> powerUpDict;
    

    Camera mainCamera;
    List<Collider2D> disabledColliders = new List<Collider2D>();

    //minimum distance for an exchange to be detected
    public float exchageDecetionDistance = .65f;
    //when swiping a piece, expand its collider in order not to lose raycast detection
    public readonly short movingColliderSize = 8;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        // TODO dependencies better as explicit and serialized?
        mainCamera = Camera.main;
        //move the camera to the center of the board
        mainCamera.transform.position = new Vector3(rows/2 - .5f, columns/2 - .5f, -10);

        prefabsDict = new Dictionary<PieceType, GameObject>();
        powerUpDict = new Dictionary<PieceType, GameObject>();

        // Load prefab dictionary
        for (int i = 0; i < prefabs.Length; i++)
            prefabsDict.Add(prefabs[i].type, prefabs[i].prefab);
        for (int i = 0; i < powerUpPrefabs.Length; i++)
        {
            powerUpDict.Add(powerUpPrefabs[i].type, powerUpPrefabs[i].prefab);
            powerUpTypes.Add(powerUpPrefabs[i].type);
        }
        
        // Set current powerup
        var selectedPowerup = powerupManager.selectedPowerup;
        // TODO very brittle. Temporary solution to convert one enum into another
        switch (selectedPowerup)
        {
            case PowerupEnum.BOMB:
                currentPowerUp = PieceType.BOMB;
                break;
            case PowerupEnum.FREEZE:
                currentPowerUp = PieceType.FREEZE;
                break;
        }

        pieces = new Piece[rows, columns];
        FillWithRandomPieces();
        // TODO a better initialization could be to start from a deadlocked state, running a fast version of Solve (no animations) that shows the pieces at the end
        StartCoroutine(Solve());
    }


#if UNITY_EDITOR
    //DEBUG
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            foreach (Piece p in pieces)
                Destroy(p.gameObject);
            FillWithRandomPieces();
        }
            
        if(Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log($"Deadlock: {CheckDeadlock()}");
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log($"Pezzi combaciano: {DEBUGGridPosMatchesArray()}");
        }
        if (Input.GetKeyDown(KeyCode.S))
            StartCoroutine(Solve());
    }


    private string DEBUGGridPosMatchesArray()
    {
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if (x != pieces[x, y].gridPos.x || y != pieces[x, y].gridPos.y)
                    return $"NO, {x}, {y}";
            }
        }
        return "YES";
    }

#endif

    #region DisableColliders

    /* Methods useful when swiping a piece:
     * all near colliders must be disabled in order to avoid raycast problems
     */

    private void DisableCollider(int x, int y)
    {
        Collider2D col = pieces[x, y].gameObject.GetComponent<Collider2D>();
        col.enabled = false;
        disabledColliders.Add(col);
    }

    public void DisableCollidersAround(int x, int y)
    {
        Debug.Assert(x >= 0 && x < rows && y >= 0 && y < columns);

        if (x > 0)
        {
            //disable collider to left
            DisableCollider(x - 1, y);
        }
        if (x < columns - 1)
        {
            //disable collider right
            DisableCollider(x + 1, y);
        }
        if (y > 0)
        {
            //disable collider to below
            DisableCollider(x, y - 1);
        }
        if (y < rows - 1)
        {
            //disable collider to above
            DisableCollider(x, y + 1);
        }
    }

    public void EnableDisabledColliders()
    {
        for (int i = 0; i < disabledColliders.Count; i++)
            disabledColliders[i].enabled = true;
        disabledColliders.Clear();
    }

    #endregion

    //Returns true when a piece is allowed to move in direction "direction"
    //(the pieces on the edges can't move outside of the board)
    public bool IsDirectionMovementAllowed(Piece piece, Direction direction)
    {
        return !(piece.gridPos.x == 0 && direction == Direction.Left ||
            piece.gridPos.x == columns - 1 && direction == Direction.Right ||
            piece.gridPos.y == 0 && direction == Direction.Down ||
            piece.gridPos.y == rows - 1 && direction == Direction.Up);
    }

    //Exchange a piece with the one in "direction"
    //(invoked when user swipes a piece)
    public void Exchange(Piece piece, Direction direction)
    {
        Debug.Assert(((piece.gridPos.x == 0 && direction != Direction.Left) ||
                     (piece.gridPos.x == columns - 1 && direction != Direction.Right) ||
                     piece.gridPos.x > 0) &&
                     ((piece.gridPos.y == 0 && direction != Direction.Down) ||
                     (piece.gridPos.y == rows - 1 && direction != Direction.Up) ||
                     piece.gridPos.y > 0)
                     );

        Vector2 piece1NewPos = piece.gridPos + Piece.DirectionToVector(direction);
        Vector2 piece2NewPos = piece.gridPos;

        //get the piece in "direction"
        Piece piece2 = pieces[(int) piece1NewPos.x, (int) piece1NewPos.y];

        //if the two pieces have the same type, exchanging them doesn't make sense
        if(piece.type == piece2.type)
        {
            CoroutineAnimation.SmoothStepToTarget(piece.transform, piece2NewPos, .2f, () => piece.spriteRenderer.sortingOrder = 0);
            return;
        }

        //otherwise exchange them
        pieces[(int)piece.gridPos.x, (int)piece.gridPos.y] = piece2;
        pieces[(int)piece2.gridPos.x, (int)piece2.gridPos.y] = piece;
        piece.gridPos = piece1NewPos;
        piece2.gridPos = piece2NewPos;

        //check matches
        MatchResult match1 = CheckMatch((int) piece.gridPos.x, (int) piece.gridPos.y);
        MatchResult match2 = CheckMatch((int)piece2.gridPos.x, (int)piece2.gridPos.y);

        Debug.Log(match1.type);
        Debug.Log(match2.type);

        if(match1.type == MatchType.None && match2.type == MatchType.None)
        {
            //There is no match, reset pieces position and return
            piece.gridPos = piece2NewPos;
            piece2.gridPos = piece1NewPos;
            pieces[(int)piece.gridPos.x, (int)piece.gridPos.y] = piece;
            pieces[(int)piece2.gridPos.x, (int)piece2.gridPos.y] = piece2;
            CoroutineAnimation.SmoothStepToTarget(piece.transform, piece2NewPos, .2f, () => piece.spriteRenderer.sortingOrder = 0);
            CoroutineAnimation.SmoothStepToTarget(piece2.transform, piece1NewPos, .2f);
            return;
        }

        //Exchange animation
        CoroutineAnimation.SmoothStepToTarget(piece.transform, piece1NewPos, .2f, () => piece.spriteRenderer.sortingOrder = 0);
        CoroutineAnimation.SmoothStepToTarget(piece2.transform, piece2NewPos, .2f);

        /* 
         * Start solve algorithm to solve these matches
         * and also the ones possibly created after the shifts
         * of the above pieces
         */
        StartCoroutine(Solve());
    }

    /* 
     * If there's at least one match in all the board, return it
     * otherwise return a MatchResult with type == MatchResult.None
     */
    private MatchResult IsThereAMatch()
    {
        MatchResult mr = new MatchResult(new List<Coord>(), MatchType.None);
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                mr = CheckBestMatch(x, y);
                if (mr.type != MatchType.None)
                    return mr;
            }
        }
        return mr;
    }

    // Check if the board is deadlocked
    public bool CheckDeadlock()
    {
        /*
         * If there is already a match => no deadlock
         * otherwise
         * exchange every piece with the one to the right, check for match
         * exchange every piece with the one below, check for match
         */
        if (IsThereAMatch().type != MatchType.None)
            return false;
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if(x < columns - 1)
                {
                    SwapPieces(x, y, x + 1, y);
                    MatchResult mr;
                    mr = CheckMatch(x, y);
                    if (mr.type != MatchType.None)
                    {
                        SwapPieces(x, y, x + 1, y);
                        return false;
                    }
                    mr = CheckMatch(x + 1, y);
                    if (mr.type != MatchType.None)
                    {
                        SwapPieces(x, y, x + 1, y);
                        return false;
                    }
                    SwapPieces(x, y, x + 1, y);
                }
                
                if(y < rows - 1)
                {
                    SwapPieces(x, y, x, y + 1);
                    MatchResult mr;
                    mr = CheckMatch(x, y);
                    if (mr.type != MatchType.None)
                    {
                        SwapPieces(x, y, x, y + 1);
                        return false;
                    }
                    mr = CheckMatch(x, y + 1);
                    if (mr.type != MatchType.None)
                    {
                        SwapPieces(x, y, x, y + 1);
                        return false;
                    }
                    SwapPieces(x, y, x, y + 1);
                }
            }
        }
        Debug.Log("DEADLOCK");
        return true; //No match found, deadlock!
    }

    /* Swaps two pieces
     * Only on the pieces matrix! The pieces' gridPos aren't swapped
     */
    private void SwapPieces(int x1, int y1, int x2, int y2)
    {
        Piece temp = pieces[x1, y1];
        pieces[x1, y1] = pieces[x2, y2];
        pieces[x2, y2] = temp;
    }

    

    /* Used to represent the result of a match
     * matchSequences contains the list of all the pieces involved in the match
     * type describes the match type, or if there's no match
     */
    struct MatchResult
    {
        public readonly List<Coord> matchSequences;
        public readonly MatchType type;

        public MatchResult(List<Coord> matchSequences, MatchType type)
        {
            this.matchSequences = matchSequences;
            this.type = type;
        }
    }

    enum MatchType
    {
        Horizontal,
        Vertical,
        Both,
        None,
    }

    // Check if there's a match on the piece pieces[x,y]
    MatchResult CheckMatch(int x, int y)
    {
        //The list contains the horizontal and vertical match pieces
        //If the match type == MatchType.Both, only the vertical list will contain the origin piece
        List<Coord> verticalMatchPieces = new List<Coord>();
        List<Coord> horizontalMatchPieces = new List<Coord>();
        bool addedOrigin = false;

        //Vertical match
        // TODO why check only in [-2,2]? longer sequences are still removed (with consecutive matches) but maybe this gives unexpected behavior
        for (int i = -2; i <= 2; i++)
        {
            //skip if the coord is outsite of the board
            //skip also the origin: it gets added when the first piece gets added
            if (i == 0 || y + i < 0 || y + i >= rows) continue;

            //If the found piece has the same type of the origin, add it
            if (pieces[x, y].type == pieces[x, y + i].type || powerUpTypes.Contains(pieces[x, y + i].type))
            {
                Coord coord = new Coord((ushort) x, (ushort) (y + i));
                verticalMatchPieces.Add(coord);
                
                if(!addedOrigin)
                {
                    coord = new Coord((ushort)x, (ushort)y);
                    verticalMatchPieces.Add(coord);
                    addedOrigin = true;
                }
            }
            else
            {
                //If there's a piece that breaks the sequence between the one found and the origin one: match invalid
                if (i < 0 && verticalMatchPieces.Count > 0)
                {
                    verticalMatchPieces.Clear();
                    addedOrigin = false;
                }
                else if (i > 0) //If there's a piece that breaks the sequence after the origin piece: simply stop the search in this direction
                    break;
            }
        }
        //If the pieces found are less than 3, that's not a match
        if (verticalMatchPieces.Count < 3)
        {
            verticalMatchPieces.Clear();
            addedOrigin = false;
        }


        //Horizontal match
        for (int i = -2; i <= 2; i++)
        {
            if (i == 0 || x + i < 0 || x + i >= columns) continue;

            if (pieces[x, y].type == pieces[x + i, y].type || powerUpTypes.Contains(pieces[x + i, y].type))
            {
                Coord coord = new Coord((ushort)(x + i), (ushort) y);
                horizontalMatchPieces.Add(coord);

                if (!addedOrigin)
                {
                    coord = new Coord((ushort)x, (ushort)y);
                    horizontalMatchPieces.Add(coord);
                    addedOrigin = true;
                }
            }
            else
            {
                if (i < 0 && horizontalMatchPieces.Count > 0)
                    horizontalMatchPieces.Clear();
                else if (i > 0)
                    break;
            }
        }
        // TODO why check that the horizontal match count is exactly 2? (other than <3?) and how is it possible that the vertical
        // TODO if ther's a vertical match and a horizontal match of count 2, then we're not clearing the horizontal match?
        if (horizontalMatchPieces.Count < 3 && !(verticalMatchPieces.Contains(new Coord((ushort)x, (ushort)y)) && horizontalMatchPieces.Count == 2))
        {
            horizontalMatchPieces.Clear();
            addedOrigin = false;
        }

        //Compute the match type
        MatchType matchType = MatchType.None;

        if (verticalMatchPieces.Count > 0 && horizontalMatchPieces.Count > 0)
            matchType = MatchType.Both;
        else if (verticalMatchPieces.Count > 0)
            matchType = MatchType.Vertical;
        else if (horizontalMatchPieces.Count > 0)
            matchType = MatchType.Horizontal;

        Debug.Log($"Vertical: {verticalMatchPieces.Count}, Horizontal: {horizontalMatchPieces.Count}");

        verticalMatchPieces.AddRange(horizontalMatchPieces);

        return new MatchResult(verticalMatchPieces, matchType);
    }

    /* Given piece coordinates,
     * if that piece is part of a match,
     * find if there's a piece of the same sequence that
     * has a best match (higher matchSequences length)
     * 
     * Complexity: linear in matchSequences
     */
    private MatchResult CheckBestMatch(int x, int y)
    {
        MatchResult best = CheckMatch(x, y);

        foreach(Coord coord in best.matchSequences)
        {
            MatchResult mr = CheckMatch(coord.x, coord.y);
            if (mr.matchSequences.Count > best.matchSequences.Count)
                best = mr;
        }
        return best;
    }

    // Fill the board with random pieces
    private void FillWithRandomPieces()
    {
        GameObject g;
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if(pieces[x, y])
                    Destroy(pieces[x, y].gameObject);

                Vector3 pos = new Vector3(x, y, 0);
                GameObject piece = GetRandomPiece();
                g = Instantiate(piece, pos, Quaternion.identity, this.transform);
                pieces[x, y] = g.GetComponent<Piece>();
                pieces[x, y].gridPos = pos;
            }
        }
    }

    private GameObject GetRandomPiece()
    {
        
        float extractedNumber = UnityEngine.Random.Range(0, 100);
        if (extractedNumber < powerUpProbability)
            return powerUpDict[currentPowerUp];

        return prefabs[(int)UnityEngine.Random.Range(0, prefabs.Length)].prefab;
    }

    // Place a piece of type "type" in the given (x,y) position
    void PlacePiece(int x, int y, GameObject piece)
    {
        GameObject g = Instantiate(piece, new Vector3(x, y), Quaternion.identity, this.transform);
        pieces[x, y] = g.GetComponent<Piece>();
        pieces[x, y].gridPos = new Vector3(x, y);
    }

#if UNITY_EDITOR
    void DEBUGReplacePiece(int x, int y, GameObject piece)
    {
        if(pieces[x,y])
            Destroy(pieces[x, y].gameObject);
        PlacePiece(x, y, piece);
    }
#endif

    /* Solve algorithm
     * - Solve all the matches there are in the board
     * - Make pieces above the matched pieces fall down
     * - Generates new pieces for the topmost position if left empty by previous matches
     */
    // TODO if we have time, we could develop a recursive and memoized version of this that avoids redundant match checks
    private IEnumerator Solve()
    {
        //Disable user input, we don't want problems because matches animations take time!
        ClickableHandler.active = false;
        MatchResult mr;

        /* flag that indicates when we need another iteration of the algorithm due to previous deadlock
         * (board regenerated)
         */
        bool checkAgain = true;

        while(checkAgain)
        {
            checkAgain = false;
            while ((mr = IsThereAMatch()).type != MatchType.None)
            {
                yield return new WaitForSeconds(.5f);
                yield return StartCoroutine(DestroyMatchedPieces(mr.matchSequences, PointsType.NORMAL));

                MakePiecesFall();

                //fill empty tiles with brand new pieces
                for(int x = 0; x < columns; x++)
                {
                    for (int y = 0; y < rows; y++)
                    {
                        if(pieces[x, y] == null)
                        {
                            PlacePiece(x, y, GetRandomPiece());
                            pieces[x, y].transform.position = new Vector3(x, y + rows);
                            CoroutineAnimation.SmoothStepToTarget(pieces[x, y].transform, new Vector3(x, y), .2f);
                        }
                    }
                }
            }

            while (CheckDeadlock())
            {
                FillWithRandomPieces();
                checkAgain = true;
            }
        }

        ClickableHandler.active = true; //re enable user input
    }

    private IEnumerator DestroyMatchedPieces(List<Coord> matchSequence, PointsType pointsType)
    {
        if(matchSequence != null)
        {
            float waitTime = 0;
            foreach (Coord coord in matchSequence)
                {
                    if(pieces[coord.x, coord.y]) //could be null because of recursive calls
                    {
                        IPowerUp powerUp = pieces[coord.x, coord.y].GetComponent<IPowerUp>();
                        List<Coord> destroyedByPowerUp = null;
                        if (powerUp != null)
                        {
                            destroyedByPowerUp = powerUp.Activate(matchSequence, this);
                        }

                        //animate and destroy match pieces
                        pieces[coord.x, coord.y].GetComponent<Animator>().SetTrigger("Pop"); //TODO: hash trigger string
                        waitTime = pieces[coord.x, coord.y].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
                        //yield return new WaitForSeconds(waitTime);
                        

                        //destroy powerUp-destroyed pieces before going on
                        yield return StartCoroutine(DestroyMatchedPieces(destroyedByPowerUp, PointsType.BOMB));
                    }
                    
                }
            
            yield return new WaitForSeconds(waitTime);

            foreach (Coord coord in matchSequence)
            {
                if (pieces[coord.x, coord.y])
                {
                    Destroy(pieces[coord.x, coord.y].gameObject);
                    pieces[coord.x, coord.y] = null;
                }
            }
                

            // update score
            matchEvent?.Broadcast(matchSequence.Count, pointsType);
        }
    }

    //If there are tiles without pieces, make above pieces fall
    private void MakePiecesFall()
    {
        for(int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if(!pieces[x,y])
                {
                    int i = y + 1;
                    while (i < rows && pieces[x, i] == null)
                        i++;
                    if(i != rows)
                    {
                        SwapPieces(x, y, x, i);
                        Vector3 pos = new Vector3(x, y);
                        CoroutineAnimation.SmoothStepToTarget(pieces[x, y].transform, pos, .2f);
                        pieces[x, y].gridPos = pos;
                    }
                }
            }
        }
    }

    public bool IsInGrid(int x, int y)
    {
        return x > 0 && x < columns && y > 0 && y < rows;
    }
}
