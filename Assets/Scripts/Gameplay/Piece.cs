using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

public enum Direction
{
    None,
    Up,
    Down,
    Left,
    Right,
}

//Used to lock the swipe direction when the user performs a swipe
struct PieceMovementSwipeLock
{
    private Direction direction;
    public Vector2 maxSwipePos;
    private Vector2 coordMask;

    // TODO is this the same as having coordMask readonly? (not sure)
    public Vector2 CoordMask
    {
        get { return coordMask; }
        private set { }
    }

    public Direction Direction
    {
        get { return direction; }
        set
        {
            direction = value;
            coordMask = Piece.DirectionToVector(direction).Abs();
        }
    }

    public void Unlock()
    {
        direction = Direction.None;
        maxSwipePos = Vector2.zero;
        CoordMask = Vector2.zero;
    }
}


public class Piece : MonoBehaviour, IClickable
{
    public PieceType type;
    public Vector2 gridPos;
    public float swipeDetectionThreshold = 0.1f;

    private bool firstFramePressing = true;
    private Vector2 firstPressingPos = Vector2.zero;
    private bool isSwiping = false;
    private Direction swipeDirection = Direction.None;
    private PieceMovementSwipeLock movementLock;
    private BoxCollider2D boxCollider;

    public SpriteRenderer spriteRenderer;

    //Converts direction to a Vector2
    public static Vector2 DirectionToVector(Direction direction)
    {
        switch(direction)
        {
            case Direction.Up:
                return Vector2.up;
            case Direction.Down:
                return Vector2.down;
            case Direction.Left:
                return Vector2.left;
            case Direction.Right:
                return Vector2.right;
            default:
                return Vector2.zero;
        }
    }


    void Start()
    {
        movementLock.Direction = Direction.None;
        // TODO dependencies better as explicit and serialized?
        boxCollider = GetComponent<BoxCollider2D>();
        // spriteRenderer = GetComponent<SpriteRenderer>();
    }

    #region IClickable_methods

    public void Click()
    {
        NotPressingAnymore();
    }

    public void EndPressing()
    {
        NotPressingAnymore();
    }

    //Equivalent to Input.GetKey
    public void Pressing(Vector2 pos)
    {
        if(firstFramePressing)
        {
            firstFramePressing = false;
            firstPressingPos = pos;
            boxCollider.size = Vector2.one * GridManager.Instance.movingColliderSize;
            return;
        }

        //control reaches this point from the second frame pressing on

        Vector2 movement = pos - firstPressingPos;
        isSwiping = pos != firstPressingPos;
        if (isSwiping && movement.magnitude > swipeDetectionThreshold) //is swiping
        {
            //we are sure that movement != Vector2.zero since pos != oldPressingPos
            swipeDirection = GetSwipeDirection(movement);

            if (!GridManager.Instance.IsDirectionMovementAllowed(this, swipeDirection))
                swipeDirection = Direction.None;

            //lock the direction
            if (movementLock.Direction == Direction.None && swipeDirection != Direction.None)
            {
                movementLock.Direction = swipeDirection;
                movementLock.maxSwipePos = (Vector2) transform.position + DirectionToVector(movementLock.Direction);
            }

        }

        if(isSwiping && movement.magnitude > swipeDetectionThreshold)
        {
            Moving(pos);
            GridManager.Instance.DisableCollidersAround((int) gridPos.x, (int) gridPos.y);
        }

        if (isSwiping)
            spriteRenderer.sortingOrder = 1;

        if (!isSwiping)
            firstPressingPos = pos;

    }

    #endregion

    void NotPressingAnymore()
    {
        // Detect exchange
        if ((transform.position - (Vector3)gridPos).sqrMagnitude > GridManager.Instance.exchageDecetionDistance * GridManager.Instance.exchageDecetionDistance)
        {
            //Debug.Log("detected");
            GridManager.Instance.Exchange(this, swipeDirection);
        }
        else
        {
            // if no exchange replace piece in gridPos
            CoroutineAnimation.SmoothStepToTarget(transform, gridPos, .2f, () => spriteRenderer.sortingOrder = 0);
        }

        firstFramePressing = true;
        firstPressingPos = Vector2.zero;
        swipeDirection = Direction.None;
        movementLock.Unlock();
        GridManager.Instance.EnableDisabledColliders();
        boxCollider.size = Vector2.one;
    }

    private Direction GetSwipeDirection(Vector2 movement)
    {
        bool isSwipingHorizontally = Mathf.Abs(movement.x) > Mathf.Abs(movement.y);

        if (isSwipingHorizontally)
        {
            if (movement.x > 0)
            {
                Debug.Log(Direction.Right);
                return Direction.Right;
            }
            else
            {
                Debug.Log(Direction.Left);
                return Direction.Left;
            }
        }
        else //Is swiping left or down
        {
            if (movement.y > 0)
            {
                Debug.Log(Direction.Up);
                return Direction.Up;
            }
            else
            {
                Debug.Log(Direction.Down);
                return Direction.Down;
            }
        }
    }


    Vector3 currentVelocity; //needed for piece SmoothDamp movement in Moving
    //The player is moving a piece, update piece position
    void Moving(Vector2 currentPos)
    {
        Vector2 delta = currentPos - (Vector2) transform.position;
        Vector3 newPos = transform.position + (Vector3)(delta * movementLock.CoordMask);
        newPos = new Vector3(
            Mathf.Clamp(newPos.x, Mathf.Min(gridPos.x, movementLock.maxSwipePos.x), Mathf.Max(gridPos.x, movementLock.maxSwipePos.x)),
            Mathf.Clamp(newPos.y, Mathf.Min(gridPos.y, movementLock.maxSwipePos.y), Mathf.Max(gridPos.y, movementLock.maxSwipePos.y)),
            0);

        transform.position = Vector3.SmoothDamp(transform.position, newPos, ref currentVelocity, .1f);


        /*Debug.Log($"mask: {movementLock.CoordMask}");
        Debug.Log($"delta: {(Vector3)(delta * movementLock.CoordMask)}");*/
    }
}
