using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ScoreUpdatedCallback : UnityEvent<int>
{
}
public class MonoScoreUpdatedListener : MonoBehaviour
{
    // what message do we listen for?
    // (we don't listen for a particular emitter, but for an event)
    public SOScoreUpdatedEvent Event;
    
    // serialized response
    public ScoreUpdatedCallback Response;

    public void OnEnable()
    {
        Event.RegisterListener(this);
    }

    public void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEvent(int newScore)
    {
        Response?.Invoke(newScore);
    }
}
