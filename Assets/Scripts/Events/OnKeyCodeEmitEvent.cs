using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnKeyCodeEmitEvent : MonoBehaviour
{
    public KeyCode keyCode;
    public SOEvent Event;
        
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyCode))
        {
            Event.Broadcast();
        }
    }
}
