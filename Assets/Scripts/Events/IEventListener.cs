
using UnityEngine;

// TODO I'd like to abstract away an "EventListener"
// that both MonoEventListener and SOEventListener use
// But don't know how to properly do it.
// The goal is to avoid duplication, which currently happens
// between MonoEventListener and SOEventListener.
// And I'd like to link them in code. Currently there's nothing
// codifying a relationship between the two.

// public class EventListener<EventT, ResponseT>
// {
//     public ISO0AryEvent<T> Event;
//     public ResponseT Response;
// private void OnEnable()
// {
//     Event.RegisterListener(this);
// }
//
// private void OnDisable()
// {
//     Event.UnregisterListener(this);
// }
//
// public void OnEvent()
// {
//     Response?.Invoke();
// }
// }