using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


// An event listener component for GameObjects
// You can edit the event (must be a scriptable object instance)
// and the response (UnityEvent) from the inspector,
// making this fully data-driven and editable.
public class MonoEventListener : MonoBehaviour
{
    // what message do we listen for?
    // (we don't listen for a particular emitter, but for an event)
    public SOEvent Event;
    
    // serialized response
    public UnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEvent()
    {
        Response?.Invoke();
    }
}
