using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartEmitEvent : MonoBehaviour
{
    public SOEvent Event;

    private void Start()
    {
        Event.Broadcast();
    }
}
