using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BroadcastUnityEvent : MonoBehaviour
{
    public UnityEvent Event;
    public void BroadcastBoundUnityEvent()
    {
        // Debug.Log($"{name} - emitting event");
        Event?.Invoke();
    }
}
