using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SOLoadSceneEvent)), CanEditMultipleObjects] // TODO would like to have a single custom editor for all ISO-0Ary-Events
public class SOLoadSceneEventCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // base.OnInspectorGUI();
        DrawDefaultInspector();

        if (GUILayout.Button("Broadcast"))
        {
            // serializedObject.FindProperty("Broadcast");
            var Event = (SOLoadSceneEvent)target;
            Event.Broadcast();
        }
    }
}
