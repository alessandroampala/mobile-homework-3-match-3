using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SOCinematicEvent)), CanEditMultipleObjects] // TODO would like to have a single custom editor for all ISO-0Ary-Events
public class SOCinematicEventCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // base.OnInspectorGUI();
        DrawDefaultInspector();

        if (GUILayout.Button("Broadcast"))
        {
            // serializedObject.FindProperty("Broadcast");
            var Event = (SOCinematicEvent)target;
            Event.Broadcast();
        }
    }
}
