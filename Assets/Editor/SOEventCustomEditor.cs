using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SOEvent)), CanEditMultipleObjects] // TODO would like to have a single custom editor for all ISO-0Ary-Events
public class SOEventCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // base.OnInspectorGUI();
        DrawDefaultInspector();

        if (GUILayout.Button("Broadcast"))
        {
            // serializedObject.FindProperty("Broadcast");
            var Event = (SOEvent)target;
            Event.Broadcast();
        }
    }
}
